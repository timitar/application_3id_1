@extends('layouts.dashboard.app')


@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Users <small>{{$users->total()}}</small></h3>

            <form action="{{route('dashboard.users.index')}}" method="get">

                <div class="row">
                    <div class="col-md-4">
                        <input type="text" name="search" class="form-control" placeholder="Search" value="{{request()->search}}">
                    </div>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-primary">Search</button>
                        @if(auth()->user()->hasPermission('create_users'))
                            <a href="{{route('dashboard.users.create')}}" class="btn btn-primary">Add User</a>
                        @else
                            <a href="#" class="btn bg-gradient-info disabled">Add User</a>
                        @endif

                    </div>
                </div>

            </form>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
             @if($users->count()>0)
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>first name</th>
                        <th>last name</th>
                        <th>image</th>
                        <th>email</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                       @foreach($users as $index=>$user)
                          <tr>
                              <td>{{$index + 1}}</td>
                              <td>{{$user->first_name}}</td>
                              <td>{{$user->last_name}}</td>
                              <td><img src="{{$user->image_path}}" class="img-thumbnail" style="width: 70px"></td>
                              <td>{{$user->email}}</td>
                              <td>
                                  @if(auth()->user()->hasPermission('update_users'))
                                      <a href="{{route('dashboard.users.edit',$user->id)}}" class="btn bg-gradient-info">edit</a>
                                  @else
                                      <a href="#" class="btn bg-gradient-info disabled">edit</a>
                                  @endif

                                  @if(auth()->user()->hasPermission('delete_users'))
                                          <form action="{{route('dashboard.users.destroy',$user->id)}}" method="post" style="display: inline">
                                              @csrf
                                              @method('delete')
                                              <button class="btn bg-gradient-danger delete">delete</button>
                                          </form>
                                  @else
                                          <button type="submit" class="btn bg-gradient-danger disabled">delete</button>
                                  @endif







                              </td>

                          </tr>
                        @endforeach
                    </tbody>
                </table>
             @else
                 <h2>NO data found</h2>
            @endif

        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
           {{$users->appends(request()->query())->links()}}
        </div>
    </div>
@endsection
