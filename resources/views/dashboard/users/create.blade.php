@extends('layouts.dashboard.app')


@section('content')


        <div class="card card-info col-md-6">
            <div class="card-header">
                <h3 class="card-title">Add User</h3>
            </div>

                 @include('partials._errors')

            <form action="{{route('dashboard.users.store')}}" method="post"  enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <label for="first_name" class="col-sm-3 col-form-label">First name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="last_name" class="col-sm-3 col-form-label">Last name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" name="email" value="{{old('email')}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-3 col-form-label">Photo</label>
                        <div class="col-sm-8">
                            <input type="file" class="form-control image" name="image" >
                        </div>
                    </div>

                       <div class="form-group row">
                        <div class="col-sm-8">
                            <img src="{{asset('uploads/user_images/default.png')}}" style="width: 50px" class="img-thumbnail image-preview">
                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="password" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label">Password Confirmation</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="password_confirmation" >
                        </div>
                    </div>

                </div>
                        {{--Tabs--}}
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0">
                                @php
                                $models=['users','categories','product']
                                @endphp
                                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">

                                @foreach($models as $model)
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill" href="#{{$model}}" role="tab" aria-controls="custom-tabs-four-home" aria-selected="false">{{$model}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-four-tabContent">
                                    @foreach($models as $model)
                                        <div class="tab-pane fade" id="{{$model}}" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                                            {{--permission checkbox--}}
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch1" name="permissions[]" value="create_{{$model}}">
                                                <label class="custom-control-label" for="customSwitch1">create</label>
                                            </div>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch2" name="permissions[]" value="read_{{$model}}">
                                                <label class="custom-control-label" for="customSwitch2">read</label>
                                            </div>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch3" name="permissions[]" value="update_{{$model}}">
                                                <label class="custom-control-label" for="customSwitch3">update</label>
                                            </div>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch4" name="permissions[]" value="delete_{{$model}}">
                                                <label class="custom-control-label" for="customSwitch4">delete</label>
                                            </div>
                                            {{--end permission checkbox--}}
                                        </div>
                                    @endforeach

                                  {{--  <div class="tab-pane fade" id="tabs2" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                                        test2
                                    </div>--}}
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
                        {{--end tabs--}}

                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Valider</button>
                  {{--  <button type="submit" class="btn btn-default float-right">Cancel</button>--}}
                </div>
                <!-- /.card-footer -->
            </form>
        </div>




@endsection
