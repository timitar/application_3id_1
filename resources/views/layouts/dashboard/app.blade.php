
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="lib/noty.css" rel="stylesheet">
    <title>AdminLTE 3 | Starter</title>

    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <!-- Navbar -->
  @include('layouts.dashboard._header')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
  @include('layouts.dashboard._aside')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Starter Page</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Starter Page</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="container-fluid">
            @include('partials._session')
            @yield('content')

        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
   @include('layouts.dashboard._drawer')
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
  @include('layouts.dashboard._footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<script src="{{asset('/js/app.js')}}"></script>
<script src="lib/noty.js" type="text/javascript"></script>
<script >
    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });
 /*********delete confirme***********/
     $('.delete').click(function(e){
         var that=$(this)
         e.preventDefault();
         var n= new Noty({
             text:"confirme",
             type:"warning",
             killer:true,
             buttons:[
                 Noty.button("yes",'btn btn-success mr-2', function(){
                     that.closest('form').submit();
                 }),
                 Noty.button("no",'btn btn-primary mr-2', function(){
                     n.close();
                 })

             ]
         });
         n.show();

     });
 /********************/

    /*********display image in create user***********/


    $(".image").change(function() {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.image-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]); // convert to base64 string
        }
    });
    /********************/
</script>
</body>
</html>
