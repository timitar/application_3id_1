

{{--
@if(session('success'))
   <script >
       new Noty({
           type:'success',
           layout:'topRight',
           text:"{{session('success')}}",
           timeout:'2000',
           killer:true
       }).show();

    </script>
@endif
--}}


@if (session('success'))
    <div class="alert alert-success">
        <button class="close" data-dismiss="alert">X</button>
        {{ session('success') }}
    </div>
@endif
