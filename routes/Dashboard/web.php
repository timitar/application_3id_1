<?php







Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function (){
    Route::get('/index','DashboardController@index')->name('index');
    Route::resource('users','UserController');
});
